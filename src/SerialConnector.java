import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class SerialConnector implements SerialPortEventListener {
	// private static Logger LOGGER = Logger.getLogger(SerialConnector.class);
	private static final int TIME_OUT = 2000;
	private static final int DATA_RATE = 115200;
	private static final SerialConnector CONNECTOR = new SerialConnector();
	private BufferedReader input;
	private OutputStream output;
	private SerialPort serialPort;

	public static void main(String[] args) {
		SerialConnector c = new SerialConnector();
		c.init();
	}

	private SerialConnector() {
		init();
	}

	public static SerialConnector getInstance() {
		return CONNECTOR;
	}

	public void init() {
		try {
			CommPortIdentifier commPortIdentifier = getCommPortIdentifier("COM7"); /// dev/ttyAMA0
			if (commPortIdentifier == null) {
				// LOGGER.error("Could not find COM port.");
				System.out.println("Could not find COM port.");

				return;
			}
			initialize(commPortIdentifier);
		} catch (Exception ex) {
			// LOGGER.error("Could not find COM port.");
		}
	}

	private CommPortIdentifier getCommPortIdentifier(String portName) {
		Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();
		/** First, Find an instance of serial port as set in PORT_NAMES. */
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			if (currPortId.getName().equals(portName)) {
				return currPortId;
			}
		}
		return null;
	}

	private void initialize(CommPortIdentifier commPortIdentifier) throws IOException {
		try {
			serialPort = (SerialPort) commPortIdentifier.open(this.getClass().getName(), TIME_OUT);
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
			output = serialPort.getOutputStream();// new BufferedWriter(new
													// OutputStreamWriter(serialPort.getOutputStream()));
		} catch (Exception e) {
			// LOGGER.error(e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void serialEvent(SerialPortEvent serialPortEvent) {
		if (serialPortEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			// System.out.println("SERIAL EVENT : " + serialPortEvent);

			String serialRead = null;
			String serachPattern = "BTN_";
			try {
				serialRead = input.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(serialRead);
			try {
				if (serialRead.equals("LED")) {
					System.out.println("Turn LED ON");
					String data = "y";
					output = serialPort.getOutputStream();
					output.write(data.getBytes());
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 * Pattern p = Pattern.compile(serachPattern); Matcher m =
			 * p.matcher(serialRead);
			 * 
			 * if (m.find()) { System.out.println("Button gedr?ckt");
			 * MainController.button = serialRead;
			 * NavigationMenuPanel.getInstance().makeAction(serialRead); return;
			 * }
			 * 
			 * System.out.println("SERIAL READ: " + serialRead); RequestObj
			 * requestObj = new RequestObj();
			 * 
			 * requestObj.setRequestType("RFID_VALIDATE"); Map<String, Object>
			 * params = new HashMap<String, Object>(); params.put("RFID",
			 * serialRead); params.put("REASON", MainController.reason);
			 * requestObj.setParams(params); JSONObject uiObj =
			 * AthenaServerConnector.getData(requestObj);
			 * 
			 * try { MainController.userDetailsEntity =
			 * GSON.fromJson(uiObj.getString("USER_DETAILS"),
			 * UserDetailsEntity.class); MainController.viewType =
			 * uiObj.getString("VIEW_TYPE"); } catch (JsonSyntaxException |
			 * JSONException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */
		}
	}
}